/**
 * Created by salahat on 02/08/16.
 */
(function( $ ) {
    $.fn.loadingBox = function(index,complete,send) {
        $(document)
            .ajaxStart(function () {
                $(index).append('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
            })
            .ajaxStop(function () {
                $('.overlay').fadeOut(1000, function(){ $('.overlay').remove(); });
            })
            .ajaxSend(function( event, request, settings ) {
            //$( ".msg" ).append(send);
            })
            .ajaxComplete(function( event,request, settings ) {

            $( ".msg" ).append(complete);

            $(document).unbind('ajaxStart');

            $(document).unbind('ajaxSend');

            $(document).unbind('ajaxComplete');

        });
    };
}( jQuery ));
<?php

class GeneratorsCommand extends BaseCommand
{
    // Please to use this command , run every 15 mint

    private $start_time;

    private $end_time;

    private $per_post_day;

    private $direct_push_start;

    private $direct_push_end;

    private $is_custom_settings;

    private $is_direct_push;

    private $scheduled_counter = 0;

    private $gap_time = 0;

    private $PlatFrom;

    private $rule;

    private $template;

    private $main_hash_tag;

    private $hash_tag;

    private $trim_hash;

    private $category;

    private $sub_category;

    private $settings;

    private $debug = false;

    private $image_copyright = array(' AP ',' AFP ',' Reuters ' , ' Getty Images ' , ' EPA ' , ' Action Images ',' WAM ');

    private $image_instagram_scheduled = false;


    public function run($args)
    {
        Yii::import('application.modules.features.models.*');

        system("clear");

        $this->TimeZone();

        $this->template = PostTemplate::model()->findAll('deleted = 0 ORDER BY RAND()');

        if (empty($this->template)) {

                echo '[App] : Please insert template '.PHP_EOL;

            exit();
        }

        $this->rule = Yii::app()->params['rules'];

        $d = Hashtag::model()->findAll('deleted = 0');

        $this->trim_hash = array();

        $this->hash_tag = array();

        foreach ($d as $index => $item) {
            $this->trim_hash[$index] = " #" . str_ireplace(" ", "_", trim($item->title)) . ' ';
            $this->hash_tag[$index] = ' ' . trim($item->title) . ' ';
        }


        $this->main_hash_tag = Yii::app()->params['HashTagRoot'];

        $this->category = Category::model()->findAllByAttributes(array('deleted' => 0, 'active' => 1));

        $this->sub_category = SubCategories::model()->findAllByAttributes(array('deleted' => 0, 'active' => 1));

        $this->settings = Settings::model()->findByPk(1);

        $this->PlatFrom = Platform::model()->findAllByAttributes(array('deleted' => 0));

        $this->per_post_day = $this->settings->how_many_posts;

        $this->start_time = $this->settings->start_date_time;

        $this->end_time = $this->settings->end_date_time;

        $this->gap_time = $this->settings->gap_time;

        $this->direct_push_start = $this->settings->direct_push_start;

        $this->direct_push_end = $this->settings->direct_push_end;

        $this->is_custom_settings = false;

        $this->is_direct_push = $this->settings->direct_push;


        $criteria = New CDbCriteria();

        $criteria->compare('is_scheduled', 1, true);

        $criteria->addCondition('parent_id IS NULL');

        $day = 0;

        $today = date('Y-m-d', strtotime(date('Y-m-d') . ' + ' . $day . ' day'));

        $start = date("Y-m-d H:i:s", strtotime($today . ' ' . $this->start_time));

        $end = date("Y-m-d H:i:s", strtotime($today . ' ' . $this->end_time));

        if ($end < $start)
            $criteria->addBetweenCondition('schedule_date', date('Y-m-d'), date('Y-m-d', strtotime(date('Y-m-d') . ' 1 day')), 'AND');
        else
            $criteria->compare('schedule_date', date('Y-m-d'), true);

        $this->scheduled_counter = PostQueue::model()->count($criteria);

        /** add cron*/

        if(isset($args[1]) and $args[1] == true)
            $this->debug = true;

        if(isset($args[0])){

            if ($args[0] == 'rssFeedado')
                $this->rssFeedado();
            else
                exit('[Generator] : Please add category or sub_category after command :  {[ php yiic generators rssFeedado ]} '.PHP_EOL);

        }else{
            exit('[Generator] : Please add category or sub_category after command : {[ php yiic generators rssFeedado ]} '.PHP_EOL);
        }

    }

    public function clearTag($string){

        $text = str_ireplace('# #', '#', $string);
        $text = str_ireplace('##', '#', $text);
        $text = str_ireplace('&#8220;', '', $text);
        $text = str_ireplace('&#8221;', '', $text);
        $text = str_ireplace('&#8230;', '', $text);
        $text = str_ireplace('&#x2014;', '', $text);
        $text = str_ireplace('#8211;', '', $text);
        $text = str_ireplace('&#8211;', '', $text);
        $text = str_ireplace('&nbsp;', '', $text);
        $text = str_ireplace('&#160;', '', $text);
        $text = str_ireplace('. .', '', $text);
        $text = str_ireplace('&#xf2;,', '', $text);
        $text = str_ireplace('&#8211;', '', $text);
        $text = str_ireplace('2&#xb0;', '', $text);
        return str_ireplace('&ndash;', '', $text);
    }

    private function rssFeedado(){

        if (!is_array($this->category)) {
            echo '[Database] : sub category empty check please ' . PHP_EOL;
            exit();
        }

        if (!empty($this->category)) {

            foreach ($this->category as $sub) {
                if ($sub->type == 'rss'){
                    $rss = file_get_contents($sub->url);
                    if (empty(trim($rss))) {
                        continue;
                    }
                    if (!empty($sub->url)) {
                        $xml = @simplexml_load_string($rss);
                        if (isset($xml)) {
                            foreach ($xml->channel->item as $item) {

                                if (isset($item)) {


                                    $url = null;
                                    $create = null;
                                    $date = null;

                                    $data['sub'] = $sub;

                                    $data['sub_category_id'] = null;

                                    $data['category_id'] = $sub->id;

                                    $data['creator'] = null;

                                    $data['column'] = null;

                                    $data['num_read'] = null;

                                    $data['title'] = null;

                                    $data['link'] = $url;

                                    $data['link_md5'] = md5($url);

                                    $data['description'] = null;

                                    $data['body_description'] = null;

                                    $data['shorten_url'] = null;

                                    $data['publishing_date'] = $date;

                                    $namespaces = $item->getNameSpaces(true);
                                    $dc = $item->children($namespaces['dc']);
                                    $dc_date = $item->children($namespaces['dc']);

                                    $e_content = $item->children("content", true);
                                    if (isset($e_content->encoded))
                                        $e_encoded = (string)$e_content->encoded;


                                    if (isset($dc))
                                        $data['creator'] = (string)$dc->creator;

                                    if (isset($item->title))
                                        $data['title'] = strip_tags((string)$item->title);

                                    if (isset($item->link)) {
                                        if (strpos($item->link, 'http') === 0) {
                                            $url = (string)$item->link;

                                        } else
                                            $url = Yii::app()->params['feedUrl'] . (string)$item->link;

                                        $data['link'] = $url;
                                        $data['link_md5'] = $url;
                                    }
                                    if (isset($item->description))
                                        $data['description'] = strip_tags((string)$item->description);

                                    if (isset($e_encoded))
                                        $data['body_description'] = strip_tags($e_encoded);

                                    if (isset($item->pubDate))
                                        $pub_date = (string)$item->pubDate;
                                    elseif (isset($dc_date))
                                        $pub_date = $dc_date->date;

                                    if (isset($pub_date))
                                        $data['publishing_date'] = date('Y-m-d H:i:s', strtotime(trim($pub_date)));

                                    if (isset($url) && !empty($url)) {


                                        if (!empty($data['link'])) {
                                            /*if (true) {*/
                                            list($title, $description, $body_description, $video, $image_og, $gallery, $publishing_date, $author) = $this->get_details($data['link'], $sub->id);
                                            if (!empty($title))
                                                $data['title'] = $this->clear_tags($title);
                                            if (!empty($publishing_date)) {
                                                $dates = str_replace("/", "-", $publishing_date);
                                                $date = date('Y-m-d', strtotime($dates));

                                                $data['publishing_date'] = $date;
                                            }
                                            if (!empty($description))
                                                $data['description'] = $this->clear_tags($description);
                                            if (!empty($body_description))
                                                $data['body_description'] = $this->clear_tags($body_description);
                                            if (!empty($author))
                                                $data['creator'] = $author;
                                            $data['media']['video'] = $video;
                                            $data['media']['image'] = $image_og;

                                            $data['media']['gallery'] = $gallery;

                                            $news = News::model()->findByAttributes(array('link_md5' => $data['link_md5']));
                                            /*if ($data['publishing_date'] == date('Y-m-d')) {*/

                                                if (empty($news)) {


/*                                                    $this->news_counter++;*/

                                                    $this->AddNews($data);

                                                } else
                                                    echo '[Database] : News is already exist ' . $news->id . PHP_EOL;
                                 /*           } else
                                                echo '[Website] : This news is old date : ' . $data['publishing_date'] . ' Link is : ' . $url . PHP_EOL;*/
                                        } else {
                                            echo '[link] : Empty ' . $sub->url;

                                        }


                                    } else
                                        echo '[Website] : Url empty or does not exist in the current sub category id = ' . $sub->id . PHP_EOL;

                                }
                            }
                        } else
                            echo '[Sub link] : There is no data in this sub category ' . $sub->title . PHP_EOL;


                    } else
                        echo '[Database] : Url empty in the current sub category id = ' . $sub->id . PHP_EOL;
                }
            }
        }

    }

    private function rssFeedCategory(){

        if (!is_array($this->category))
            exit('[Generator] : Category empty Please check  '.PHP_EOL);


        foreach ($this->category as $sub) {
            if ($sub->type == 'rss') {
                $url_data = file_get_contents($sub->url);

                if (empty(trim($url_data)))
                    continue;

                $xml = @simplexml_load_string($url_data);

                if ($xml)

                    if (isset($xml->channel->item)) {

                        if ($this->debug)
                            echo '[Generator] : Now read from category ' . $sub->url_rss . PHP_EOL;

                        foreach ($xml->channel->item as $item) {

                            $this->image_instagram_scheduled = false;

                            $data['sub_category_id'] = null;

                            $data['category_id'] = $sub->id;

                            $data['creator'] = null;

                            $data['column'] = null;

                            $data['num_read'] = null;

                            $data['title'] = null;

                            $data['link'] = null;

                            $data['link_md5'] = null;

                            $data['description'] = null;

                            $data['body_description'] = null;

                            $data['shorten_url'] = null;

                            $data['publishing_date'] = null;

                            $published = null;

                            $e_content = $item->children("content", true);

                            if (isset($e_content->encoded))
                                $e_encoded = (string)$e_content->encoded;

                            if (isset($item->pubDate))
                                $pub_date = (string)$item->pubDate;

                            if (isset($pub_date))
                                $published = date('Y-m-d', strtotime(trim($pub_date)));

                            if (isset($published) && $published == date('Y-m-d')) {
                                /*if (true) {*/

                                $news = News::model()->findByAttributes(array('link_md5' => $data['link_md5']));

                                if (empty($news)) {

                                    $details = $this->get_details((string)$item->link,$sub->id);

                                    if (isset($details['creator']))
                                        $data['creator'] = $details['creator'];

                                    if (isset($item->title))
                                        $data['title'] = strip_tags((string)$item->title);
                                    else
                                        $data['title'] = isset($details['title']) ? $details['title'] : null;

                                    if (isset($item->link))
                                        $data['link'] = (string)$item->link;

                                    if (isset($item->link))
                                        $data['link_md5'] = md5((string)$item->link);


                                    if (isset($item->description))
                                        $data['description'] = strip_tags((string)$item->description);
                                    else
                                        $data['description'] = isset($details['description']) ? $details : null;


                                    if (isset($e_encoded))
                                        $data['body_description'] = strip_tags($e_encoded);
                                    else
                                        $data['body_description'] = isset($details['body_description']) ? $details : null;


                                    if (isset($pub_date))
                                        $data['publishing_date'] = date('Y-m-d H:i:s', strtotime(trim($pub_date)));


                                    $data['media'] = $details['media'];

                                    if ($this->debug)
                                        echo '[Generator] : Now add new { news } : ' . $data['link_md5'] . PHP_EOL;

                                    $this->AddNews($data);
                                } else
                                    if ($this->debug)
                                        echo '[Generator] : This { news } generate before at { ' . $news->created_at . ' } : ' . $data['link_md5'] . PHP_EOL;

                            } else
                                if ($this->debug)
                                    echo '[Generator] : old date : ' . $published . PHP_EOL;

                        }
                    } else
                        if ($this->debug)
                            echo '[Generator] : Error load date: ' . $sub->url_rss . PHP_EOL;
            }
        }

    }

    private function get_details($link,$id){

        if($this->debug)
            echo '[Generator] : Now get details from '.$link.PHP_EOL;

        $html = new SimpleHTMLDOM();
        $html_url = $html->file_get_html($link);
        if(empty($html_url))
            return false;

        $page_details = PageDetails::model()->findAllByAttributes(array('source_id'=>$id));
        $author=null;
        $gallery = null;
        $publishing_date = null;
        $body_description = null;
        $description = null;
        $image = Yii::app()->params['domain']. 'image/general.jpg';;
        $video =null;
        $title=null;
        foreach($page_details as $detail) {

            //Author
            if (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'creator') {
                $author = $html_url->find($detail->predication, 0)->content;

            }
            elseif (isset($html_url->find($detail->predication, 0)->plaintext) and !empty($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'creator') {
                $author = $html_url->find($detail->predication, 0)->plaintext;
            }
            //End author

            //title
            if (isset($html_url->find($detail->predication, 0)->plaintext) and !empty($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'title') {
                $title = $html_url->find($detail->predication, 0)->plaintext;
            }
            elseif (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'title') {
                $title = $html_url->find($detail->predication, 0)->content;

            }
            //End title


            //Body desc
            if (!empty($html_url->find($detail->predication)) and $detail->pageType->title == 'body_description') {
                $body_desc = $html_url->find($detail->predication);
                if (isset($body_desc)) {
                    foreach ($body_desc as $bodies) {
                        $body_description .= $bodies->plaintext;
                    }
                }
            }elseif (!empty($html_url->find($detail->predication,0)->plaintext) and isset($html_url->find($detail->predication,0)->plaintext) and $detail->pageType->title == 'body_description') {
                $body_description = $html_url->find($detail->predication,0)->plaintext;
            }


            //End body desc

            //Description
            if (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'description') {

                $description = $html_url->find($detail->predication, 0)->content;
            }
            elseif (isset($html_url->find($detail->predication, 0)->plaintext) and !empty($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'description') {
                $description = $html_url->find($detail->predication, 0)->plaintext;

            }
            //End Description

            //Publishing date
            if (isset($html_url->find($detail->predication, 0)->datetime) and !empty($html_url->find($detail->predication, 0)->datetime) and $detail->pageType->title == 'publishing_date') {
                $publishing_date = $html_url->find($detail->predication, 0)->datetime;
            }
            if (isset($html_url->find($detail->predication, 0)->plaintext) and !empty($html_url->find($detail->predication, 0)->plaintext) and $detail->pageType->title == 'publishing_date') {
                $publishing_date = $html_url->find($detail->predication, 0)->plaintext;
            } elseif (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'publishing_date') {
                $publishing_date = $html_url->find($detail->predication, 0)->content;
            }
            //End publishing date

            //All media
            if (!empty($html_url->find($detail->predication)) and $detail->pageType->title == 'all_media') {
                $script = $html_url->find($detail->predication);
                if (!empty($script)) {
                    foreach ($script as $sc) {
                        $media = (array)json_decode($sc->innertext);
                    }
                    if (!empty($media)) {
                        if (isset($media['videos'])) {
                            $video = $media['videos'][0];
                        }
                        if (isset($media['images'])) {
                            foreach ($media['images'] as $gall) {
                                $image = 'http://dev.ado.ae/wp' . $media['images'][0];
                                if ($gall != $media['images'][0])
                                    $gallery[] = 'http://dev.ado.ae/wp' . $gall;
                            }
                        }
                    }
                }
            }
            //End all media

            //image
            if (isset($html_url->find($detail->predication, 0)->src) and !empty($html_url->find($detail->predication, 0)->src) and $detail->pageType->title == 'image') {
                if (strpos($html_url->find($detail->predication, 0)->src, 'http') === 0) {
                    $image = $html_url->find($detail->predication, 0)->src;

                }else
                    $image = 'http://dev.ado.ae/wp'.$html_url->find($detail->predication, 0)->src;
            } elseif (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'image') {
                if (strpos($html_url->find($detail->predication, 0)->content, 'http') === 0)
                    $image = $html_url->find($detail->predication, 0)->content;
                else
                    $image = 'http://dev.ado.ae/wp'.$html_url->find($detail->predication, 0)->content;

            }
            //end image

            //video
            if (isset($html_url->find($detail->predication, 0)->src) and !empty($html_url->find($detail->predication, 0)->src) and $detail->pageType->title == 'video') {
                $video = $html_url->find($detail->predication, 0)->src;
            } elseif (isset($html_url->find($detail->predication, 0)->content) and !empty($html_url->find($detail->predication, 0)->content) and $detail->pageType->title == 'video') {
                $video = $html_url->find($detail->predication, 0)->content;

            }
            //end video

            //gallery
            if (!empty($html_url->find($detail->predication)) and $detail->pageType->title == 'gallery') {
                $gall = $html_url->find($detail->predication);
                foreach ($gall as $item) {
                    if (strpos($html_url->find($detail->predication, 0)->src, 'http') === 0) {
                        if($image != $item->src)
                            $gallery[] =$item->src;
                    }else {
                        if ($image != 'http://www.emaratalyoum.com' . $item->src)
                            $gallery[] = 'http://www.emaratalyoum.com' .$item->src;
                    }
                }

            }
            //End gallery
        }

        return array($title,$description,$body_description,$video,$image,$gallery,$publishing_date,$author);
    }

    private function custom_settings($times,$platform_id){

        $this->is_custom_settings =true;

        $condition = new CDbCriteria();

        $condition->order = 'id Desc';

        $condition->limit = 1;

        $condition->condition = 'is_scheduled = 1 and settings="custom" and platform_id = '.$platform_id;

        $check = PostQueue::model()->find($condition);

        $final_date = strtotime(date('Y-m-d H:i:s'));

        $is_scheduled = 0;

        $obj = null ;



        foreach ($times as $timeItem) {

            $timeItem=(object)$timeItem;

            $obj = $timeItem->obj_id;

            $start = strtotime(date('Y-m-d') . ' ' . $timeItem->start_time);

            $end = strtotime(date('Y-m-d') . ' ' . $timeItem->end_time);

            $now = strtotime(date('Y-m-d H:i:s') . ' + ' . $timeItem->gap_time . ' minutes');


            if ($end < $start) {
                $extra_day = 1;
                $end = strtotime(date('Y-m-d') . ' ' . $timeItem->end_time . ' + ' . $extra_day . ' day');
            }

            if ($timeItem->is_direct_push) {

                $direct_start = strtotime(date('Y-m-d') . ' ' . $timeItem->direct_push_start_time);
                $direct_end = strtotime(date('Y-m-d') . ' ' . $timeItem->direct_push_end_time);

                if ($direct_end < $direct_start) {
                    $extra_day = 1;
                    $direct_end = strtotime(date('Y-m-d') . ' ' . $timeItem->direct_push_end_time . ' + ' . $extra_day . ' day');
                }
                $now = strtotime(date('Y-m-d H:i:s'));
                if (($now >= $direct_start) && ($now <= $direct_end)) {
                    $final_date = strtotime(date('Y-m-d H:i:s') . ' - 1 minute');
                    $is_scheduled = 1;
                    break;
                }

            }


            if (empty($check)) {
                if (($now <= $start)) {
                    $final_date = $start;
                    $is_scheduled = 1;

                } elseif ($now <= $end) {
                    $final_date = $now;
                    $is_scheduled = 1;

                }
            } else {
                $date = strtotime($check->schedule_date . ' + ' . $timeItem->gap_time . ' minutes');
                if ($date < $now) {
                    $date = $now;
                }

                if (($date <= $start)) {
                    $final_date = $start;
                    $is_scheduled = 1;

                } elseif ($date <= $end) {
                    $final_date = $date;
                    $is_scheduled = 1;

                }
            }



        }


        $post_date = date('Y-m-d H:i:s', $final_date);

        return array($post_date, $is_scheduled ,$obj);

    }

    private function date($cat_id,$platform_id)
    {

        $times=  Yii::app()->db->createCommand("
                                      SELECT
                platform_category_settings.obj_id,
                platform_category_settings.platform_id,
                time_settings.start_time,
                time_settings.end_time,
                time_settings.is_direct_push,
                time_settings.direct_push_start_time,
                time_settings.direct_push_end_time,
                time_settings.gap_time
                FROM platform_category_settings
                
                INNER JOIN days_settings
                ON days_settings.platform_category_settings_id =platform_category_settings.id
                
                INNER JOIN day
                ON days_settings.day_id =day.id
                
                INNER JOIN time_settings
                ON time_settings.platform_category_settings_id =platform_category_settings.id
                INNER JOIN settings_obj
                ON settings_obj.id =platform_category_settings.obj_id
                
                 WHERE 
                 ( platform_category_settings.category_id = ".$cat_id." or platform_category_settings.category_id IS NULL )  
                 and 
                 platform_category_settings.platform_id = ".$platform_id."
                 and
                 day.title = DAYNAME(NOW())
                 and 
                 settings_obj.active = 1
                 ORDER BY category_id desc;
        ")->queryAll();


        if(!empty($times)){

            if($this->debug)
                echo '[App] : Custom Settings Platform id :  '.$platform_id.PHP_EOL;

            list($time,$is_scheduled ,$obj) =  $this->custom_settings($times,$platform_id);

            if($this->debug)
                echo '[App] : scheduled is '.$is_scheduled?'True':'false'.PHP_EOL;

            return array($time,$is_scheduled ,$obj);

        }

        $this->is_custom_settings = false;

        $final_date = null;

        $condition = new CDbCriteria();

        $condition->order = 'id Desc';

        $condition->limit = 1;

        $condition->condition = 'is_scheduled = 1 and settings="general" and platform_id = '.$platform_id;


        $check = PostQueue::model()->find($condition);
        $is_scheduled = 1;
        $now = strtotime(date('Y-m-d H:i:s') . ' + ' . $this->gap_time . ' minutes');
        $start = strtotime(date('Y-m-d') . ' ' . $this->start_time);
        $end = strtotime(date('Y-m-d') . ' ' . $this->end_time);

        if ($end < $start) {
            $extra_day = 1;
            $end = strtotime(date('Y-m-d') . ' ' . $this->end_time . ' + ' . $extra_day . ' day');
        }

        if (empty($check)) {
            if (($now <= $start)) {
                $final_date = $start;

            } elseif ($now <= $end) {
                $final_date = $now;
            } else {
                $final_date = $now;
                $is_scheduled = 0;
            }
        } else {
            $date = strtotime($check->schedule_date . ' + ' . $this->gap_time . ' minutes');
            if ($date < $now) {
                $date = $now;
            }

            if (($date <= $start)) {
                $final_date = $start;
            } elseif ($date <= $end) {
                $final_date = $date;
            } else {
                $final_date = $date;
                $is_scheduled = 0;
            }
        }

        if ($this->is_direct_push) {
            $direct_start = strtotime(date('Y-m-d') . ' ' . $this->direct_push_start);
            $direct_end = strtotime(date('Y-m-d') . ' ' . $this->direct_push_end);

            if ($direct_end < $direct_start) {
                $extra_day = 1;
                $direct_end = strtotime(date('Y-m-d') . ' ' . $this->direct_push_end . ' + ' . $extra_day . ' day');
            }
            $now = strtotime(date('Y-m-d H:i:s'));
            if (($now >= $direct_start) && ($now <= $direct_end)) {
                $final_date = strtotime(date('Y-m-d H:i:s') . ' -1 minute');
                $is_scheduled = 1;
            }
        }

        $post_date = date('Y-m-d H:i:s', $final_date);


        $times=  Yii::app()->db->createCommand("SELECT  platform_category_settings.obj_id FROM platform_category_settings
                INNER JOIN settings_obj ON settings_obj.id =platform_category_settings.obj_id
                 WHERE 
                 ( platform_category_settings.category_id = ".$cat_id." or platform_category_settings.category_id IS NULL )  
                 and 
                 platform_category_settings.platform_id = ".$platform_id."
                 and 
                 settings_obj.active = 1
        ")->queryAll();

        if($this->debug)
            echo '[App] : General Settings Platform id :  '.$platform_id.PHP_EOL;

        if(!empty($times))
            $is_scheduled = 0;

        if($this->debug)
            if(!empty($times))
                echo '[App] : This platform there are custom settings '.$platform_id.PHP_EOL;


        if($this->debug)
            if($is_scheduled)
                echo '[App] : scheduled is True'.PHP_EOL;
            else
                echo '[App] : scheduled is False'.PHP_EOL;


        return array($post_date, $is_scheduled ,false);

    }

    private function AddNews($data)
    {
        $news = new News();

        $news->id = null;

        $news->link = $data['link'];

        $news->link_md5 = $data['link_md5'];

        $news->category_id = $data['category_id'];

        $news->sub_category_id = $data['sub_category_id'];

        $news->title = $data['title'];

        $news->column = $data['column'];

        $news->description = $data['description'];

        $news->publishing_date = $data['publishing_date'];

        $news->schedule_date = $data['publishing_date'];

        $news->created_at = date('Y-m-d H:i:s');

        $news->creator = $data['creator'];

        $shortUrl = new ShortUrl();

        $news->shorten_url = $shortUrl->short(urldecode($news->link));

        $news->setIsNewRecord(true);

        if ($news->save(true)) {

            $id = null;

            if (isset($data['media']['video']))
                $id = $this->AddMediaNews($news->id, 'video', $data['media']['video']);

            if (isset($data['media']['image']))
                $id = $this->AddMediaNews($news->id, 'image', $data['media']['image']);

            if (isset($data['media']['gallery']))
                $id = $this->AddMediaNews($news->id, 'gallery', $data['media']['gallery']);

            $this->newsGenerator($news, isset($data['media']['gallery']) ? 'gallery' : 'image',$id);

        }

    }

    public function AddMediaNews($news_id, $type, $data)
    {


        $id = null;

        $find = true;

        if ($type == 'gallery') {
            foreach ($data as $item) {
                if(isset($item['caption']) && !empty($item['caption']) && $find){
                    foreach($this->image_copyright as $query) {
                        if(!empty(stristr($item['caption'], trim($query)))){
                            $find = false;
                            $this->image_instagram_scheduled = true;
                        }
                    }
                }
                if (empty(MediaNews::model()->findByAttributes(array('media' => $item['src'], 'news_id' => $news_id, 'type' => $type)))) {
                    $media = new MediaNews();
                    $media->type = $type;
                    $media->media = $item['src'];
                    $media->news_id = $news_id;
                    $media->setIsNewRecord(true);
                    $media->save(false);
                    if ($id == null)
                        $id = $media->id;
                }
            }
            return $id;
        }

        if (empty(MediaNews::model()->findByAttributes(array('media' => $data, 'news_id' => $news_id, 'type' => $type)))) {
            $media = new MediaNews();
            $media->type = $type;
            $media->media = $data;
            $media->news_id = $news_id;
            $media->setIsNewRecord(true);
            $media->save(false);
            if ($id == null)
                $id = $media->id;
        }
        return $id;


    }

    protected function newsGenerator($news, $type,$id){

        $this->scheduled_counter++;

        $parent = null;

        $category = null;

        if(isset($news->category->title))
            $category = '#' . trim(str_ireplace(array('!', '"', ':', '.', '..', '...', ' '), '_', $news->category->title));

        $sub_category = null;

        if(isset($news->subCategory->title))
            $sub_category = '#' . trim(str_ireplace(array('!', '"', ':', '.', '..', '...', ' '), '_', $news->subCategory->title));


        foreach ($this->PlatFrom as $item) {

            $media = null;

            $temp = $this->get_template($item, $news->category_id, $type);

            $title = $news->title;

            $description = $news->description;

            if ($item->title != 'Instagram') {
                $title = str_ireplace($this->hash_tag, $this->trim_hash, $news->title);
                $description = str_ireplace($this->hash_tag, $this->trim_hash, $news->description);
            }

            $title = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $title);

            $title = preg_replace('/\n(\s*\n){2,}/', "\n\n", $title);

            $description = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $description);

            $description = preg_replace('/\n(\s*\n){2,}/', "\n\n", $description);

            $shorten_url = $news->shorten_url;

            $full_creator = $news->creator;

            $text = str_ireplace(
                array('[title]', '[description]', '[short_link]', '[author]'),
                array($title, $description, $shorten_url, $full_creator),
                $temp['text']
            );

            if ($item->title == 'Facebook' or $item->title == 'Twitter') {

                $text = str_ireplace('# ', '#', $text);

                $found = true;

                preg_match_all("/#([^\s]+)/", $text, $matches);

                if (isset($matches[0])) {
                    $matches[0] = array_reverse($matches[0]);
                    $count = 0;
                    foreach ($matches[0] as $hashtag) {
                        if ($count >= 2) {
                            $found = false;
                            $text = str_ireplace($hashtag, str_ireplace('_', ' ', str_ireplace('#', '', $hashtag)), $text);
                        }
                        $count++;
                    }

                    if ($count >= 2)
                        $found = false;
                }

                if ($found)
                    $text = str_ireplace(array('[section]', '[sub_section]'), array($category, $sub_category,), $text);
                  else
                    $text = str_ireplace(array('[section]', '[sub_section]', '|'), array('', '', '',), $text);


            } elseif ($item->title == 'Instagram')

                $text = str_ireplace(array('[section]', '[sub_section]', '|'), array('', '', ''), str_ireplace('# ', '#', $text));



            $newsMedia = MediaNews::model()->findByPk($id);

            if(!empty($newsMedia))
                $media = $newsMedia->media;


            list($time,$is_scheduled ,$obj) = $this->date($news->category_id,$item->id);

            if($this->debug)
                echo '[Generator] : Now generate '.$item->title.' , Time : '.$time.PHP_EOL;


            if ($item->title == 'Twitter') {

                $text_twitter = $text;
                if ($temp['type'] == 'Preview')
                    if (!preg_match('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!', $text, $matches))
                        $text = $text . PHP_EOL . $news->shorten_url;


                if ($this->getTweetLength($text_twitter, $temp['type'] == 'Image' ? true : false, $temp['type'] == 'Video' ? true : false) > 141) {
                    $is_scheduled = 0;
                } else {
                    $text = $text_twitter . PHP_EOL;
                    if ($temp['type'] == 'Preview')
                        if (!preg_match('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!', $text, $matches))
                            $text .= PHP_EOL . $news->shorten_url;
                }
            }


            $text = $this->clearTag($text);

            if ($this->scheduled_counter > $this->per_post_day) {

                if($this->debug)
                    echo '[Generator] : news today '.$this->scheduled_counter.' , { Time }'.$this->per_post_day.PHP_EOL;

                $is_scheduled = 0;
            }

            if ($item->title != 'Instagram') {

                $text = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $text);
                $text = preg_replace('/\n(\s*\n){2,}/', "\n\n", $text);


                    if($this->image_instagram_scheduled){

                        if($this->debug)
                            echo '[Generator] : is scheduled after instagram : '.$is_scheduled.PHP_EOL;

                        $this->image_instagram_scheduled = true;

                    }

            }


            if($this->debug)
                    echo '[Generator] : is scheduled  : '.$item->title.' '.$is_scheduled.PHP_EOL;


            if($this->image_instagram_scheduled)
                $is_scheduled = 0;

            $PostQueue = new PostQueue();
            $PostQueue->setIsNewRecord(true);
            $PostQueue->id = null;
            $PostQueue->command = false;
            $PostQueue->type = $temp['type'];
            $PostQueue->post = trim($text);
            $PostQueue->schedule_date = $time;
            $PostQueue->catgory_id = $news->category_id;
            $PostQueue->main_category_id = $obj ? $obj :null;
            $PostQueue->link = $news->shorten_url;
            $PostQueue->is_posted = 0;
            $PostQueue->news_id = $news->id;
            $PostQueue->post_id = null;
            $PostQueue->media_url = $media;
            $PostQueue->settings = $this->is_custom_settings ? 'custom' : 'general';
            $PostQueue->is_scheduled = $is_scheduled;
            $PostQueue->platform_id = $item->id;
            $PostQueue->generated = 'auto';
            $PostQueue->created_at = date('Y-m-d H:i:s');

            if ($parent == null) {

                $PostQueue->parent_id = null;

                if ($PostQueue->save())
                    $parent = $PostQueue->id;
                else
                    $this->send_email('Error on GeneratorsCommand.php ', 'error on post queue save  <a href="' . Yii::app()->params['domain'] . '/postQueue/' . $PostQueue->id . '"></a>');

            } else {
                $PostQueue->parent_id = $parent;
                if (!$PostQueue->save())
                    $this->send_email('Error on GeneratorsCommand.php ', 'error on post queue save  <a href="' . Yii::app()->params['domain'] . '/postQueue/' . $PostQueue->id . '"></a>');
            }


        }

        $news->generated = 1;
        if (!$news->save())
            $this->send_email('Error on GeneratorsCommand.php ', 'error on post queue save  <a href="' . Yii::app()->params['domain'] . '/news/' . $news->id . '"></a>');


    }

    private function get_template($platform, $category, $type)
    {

        $temp = PostTemplate::model()->findByAttributes(array(
            'platform_id' => $platform->id,
            'catgory_id' => $category,
        ));

        if (!empty($temp))
            return $temp;

        $cond = new CDbCriteria();
        $cond->order = 'RAND()';
        $cond->condition = '( ( platform_id = ' . $platform->id . ' or platform_id is NUll ) and  catgory_id is NULL )';
        $temp = PostTemplate::model()->find($cond);



        if (!empty($temp))
            return $temp;

        if ($platform->title == 'Instagram') {
            isset($temp->id) ? $temp->id : null;
        }

        return Yii::app()->params['templates'];
    }


    function strposa($haystack, $needle, $offset=0) {
        if(!is_array($needle)) $needle = array($needle);
        foreach($needle as $query) {
            if(!empty(stristr($haystack, trim($query)))) return true; // stop on first true result
        }
        return false;
    }


}
<?php
/* @var $this EmailsController */
/* @var $model Emails */

$this->pageTitle = "Emails | Admin";

$this->breadcrumbs=array(
	'Emails'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Emails', 'url'=>array('index')),
	array('label'=>'Create Emails', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('#form-visible').change(function(){
	$(this).submit();
	return true;
});
$('.search-form form').submit(function(){
	$('#emails-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<style>
	.checkbox {
		display: block;
		min-height: 0px;
		margin-top: 4px;
		margin-bottom: 0px;
		padding-left: 17px;
	}
	.input-group-addon input[type=radio], .input-group-addon input[type=checkbox] {
		margin-top: 0;
	}
	.radio input[type=radio], .radio-inline input[type=radio], .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox] {
		float: left;
		margin-left: -16px;
	}
</style>
<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<div class="col-sm-9"></div>
					<div class="col-sm-3" style="text-align: left;">
					<?php echo Yii::app()->params['statement']['previousPage']; ?>




					</div>
				</div>
				<div class="box-body">
					<h1>Quick create</h1>
					<?PHP //echo $this->renderPartial('_search',array('model'=>$model),true) ?>
					<?php
					$this->renderPartial('_form', array('model'=>$model_create)); ?>


					<?PHP
					$form=$this->beginWidget('booster.widgets.TbActiveForm', array(
						'action'=>Yii::app()->createUrl($this->route),
						'method'=>'get',
						'id'=>'form-visible',
					));
					?>
					<div class="col-sm-2 pull-left page-sizes"  >

						<?php echo $form->dropDownListGroup(
							$model,
							'pagination_size',
							array(

								'widgetOptions'=>array(
									'data'=>$model->pages_size(),
									'htmlOptions'=>array(

									),
								),
								'hint'=>''
							)
						); ?>
					</div>
					<?php
					$this->endWidget();
					$this->widget('booster.widgets.TbGridView', array(
						'id'=>'emails-form',
						'type' => 'striped bordered condensed',
						'template' => '{items}{summary}{pager}',
						'enablePagination' => true,
						'pager' => array(
							'class' => 'booster.widgets.TbPager',
							'nextPageLabel' => 'Next',
							'prevPageLabel' => 'Previous',
							'htmlOptions' => array(
								'class' => 'pull-right'
							)
						),
						'htmlOptions' => array(
							'class' => 'table-responsive'
						),
						'dataProvider' => $model->search(),

						'filter' => $model,
						'columns' => array(
						/*	array(
								'name'=>'id',
								'visible'=>$model->visible_id?true:false,
							),*/


							array(
								'class' => 'booster.widgets.TbEditableColumn',
								'name' => 'f_name',
								'sortable' => true,
								'editable' => array(
									'url' => $this->createUrl('emails/edit'),
									'placement' => 'right',
									'inputclass' => 'span1'
								),
								'visible'=>$model->visible_f_name?true:false,
							),
							array(
								'class' => 'booster.widgets.TbEditableColumn',
								'name' => 'l_name',
								'sortable' => true,
								'editable' => array(
									'url' => $this->createUrl('emails/edit'),
									'placement' => 'right',
									'inputclass' => 'span1'
								),
								'visible'=>$model->visible_f_name?true:false,
							),
							array(
								'class' => 'booster.widgets.TbEditableColumn',
								'name' => 'email',
								'sortable' => true,
								'editable' => array(
									'url' => $this->createUrl('emails/edit'),
									'placement' => 'right',
									'inputclass' => 'span1'
								),
								'visible'=>$model->visible_f_name?true:false,
							),

						/*	array(
								'name'=>'l_name',
								'visible'=>$model->visible_l_name?true:false,

							),

							array(
								'name'=>'email',
								'visible'=>$model->visible_email?true:false,

							),*/
					/*		array(
								'name'=>'created_at',
								'visible'=>$model->visible_created_at?true:false,
							),*/

							array(
								'class' => 'booster.widgets.TbButtonColumn',
								'header' => 'Options',
								'template' => '{view}{update}{delete}',
								'buttons' => array(
									'view' => array(
										'label' => 'View',
										'icon' => 'fa fa-eye',
									),
									'update' => array(
										'label' => 'Update',
										'icon' => 'fa fa-pencil-square-o',
									),
									'delete' => array(
										'label' => 'Delete',
										'icon' => 'fa fa-times',

									),
								)
							),
						)));?>
				</div>
			</div>
		</div>
	</div>
</section>
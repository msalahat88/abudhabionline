<?php

class m160818_064301_alterType extends CDbMigration
{
	public function up()
	{
	    $this->execute('ALTER TABLE `category` ADD `type` ENUM(\'rss\',\'dom\') NULL DEFAULT NULL AFTER `active`;
');
	}

	public function down()
	{
		echo "m160818_064301_alterType does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
<?php

class m160724_215733_new_fields extends CDbMigration
{
	public function up()
	{
		$this->execute("ALTER TABLE  `category` ADD  `url_rss` VARCHAR( 255 ) NULL AFTER  `active` ;");
		$this->execute("ALTER TABLE  `sub_categories` ADD  `url_rss` VARCHAR( 255 ) NULL AFTER  `active` ;");
		$this->execute("ALTER TABLE  `news` ADD  `body_description` TEXT NULL AFTER  `description` ;");
	}

	public function down()
	{
		echo "m160724_215733_new_fields does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
<?php
/* @var $this InstagramAccountController */
/* @var $model InstagramAccount */

$this->breadcrumbs=array(
	'Instagram Accounts'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List InstagramAccount', 'url'=>array('index')),
	array('label'=>'Create InstagramAccount', 'url'=>array('create')),
	array('label'=>'Update InstagramAccount', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete InstagramAccount', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage InstagramAccount', 'url'=>array('admin')),
);
?>

<h1>View InstagramAccount #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'link_page',
		'username',
		'password',
		'is_general',
	),
)); ?>

<?php
/* @var $this SectionPlatformAccountController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Section Platform Accounts',
);

$this->menu=array(
	array('label'=>'Create SectionPlatformAccount', 'url'=>array('create')),
	array('label'=>'Manage SectionPlatformAccount', 'url'=>array('admin')),
);
?>

<h1>Section Platform Accounts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

<?php

/**
 * This is the model class for table "twitter_account".
 *
 * The followings are the available columns in table 'twitter_account':
 * @property integer $id
 * @property string $link_page
 * @property string $twitter_key
 * @property string $secret
 * @property string $token
 * @property integer $is_general
 * @property string $token_secret
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class TwitterAccount extends BaseModels
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'twitter_account';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('link_page, twitter_key, secret, token, token_secret', 'required'),
			array('is_general', 'numerical', 'integerOnly'=>true),
			array('link_page, twitter_key, secret, token', 'length', 'max'=>255),
			array('token_secret', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, link_page, twitter_key, secret, token, is_general, token_secret,updated_by,created_by,updated_at,created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'link_page' => 'Link Page',
			'twitter_key' => 'Twitter Key',
			'secret' => 'Secret',
			'token' => 'Token',
			'is_general' => 'Is General',
			'token_secret' => 'Token Secret',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('link_page',$this->link_page,true);
		$criteria->compare('twitter_key',$this->twitter_key,true);
		$criteria->compare('secret',$this->secret,true);
		$criteria->compare('token',$this->token,true);
		$criteria->compare('is_general',$this->is_general);
		$criteria->compare('token_secret',$this->token_secret,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TwitterAccount the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

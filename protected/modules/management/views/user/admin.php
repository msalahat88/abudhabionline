<?php
/* @var $this UserController */
/* @var $model User */

$this->pageTitle = "Users";

$this->breadcrumbs=array(
	'Management'=>array('/'.$this->module->id),
	'Users'=>array('index'),
	'Manage',
);



Yii::app()->clientScript->registerScript('search', "
$('#form-visible').change(function(){
	$(this).submit();
	return true;
});
$('.search-form form').submit(function(){
	$('#user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>	<style>
    .checkbox {
        display: block;
        min-height: 0px;
        margin-top: 4px;
        margin-bottom: 0px;
        padding-left: 17px;
    }
    .input-group-addon input[type=radio], .input-group-addon input[type=checkbox] {
        margin-top: 0;
    }
    .radio input[type=radio], .radio-inline input[type=radio], .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox] {
        float: left;
        margin-left: -16px;
    }
</style>

<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<div class="col-sm-9"> <?PHP
						$this->widget(
							'booster.widgets.TbButtonGroup',
							array(
								'size' => 'small',
								'context' => 'info',
								'buttons' => array(
									array(
										'label' => 'Create',
										'buttonType' =>'link',
										'url' => array('user/create')
									),
								),
								/*'htmlOptions'=>array(
                                    'class'=>'pull-right	'
                                )*/
							)
						);

						?></div>
					<div class="col-sm-3" style="text-align:left;">
						<?php echo Yii::app()->params['statement']['previousPage']; ?>




					</div>
				</div>
				<div class="box-body">
					<?PHP //echo $this->renderPartial('_search',array('model'=>$model),true) ?>

					<?PHP
					$form=$this->beginWidget('booster.widgets.TbActiveForm', array(
						'action'=>Yii::app()->createUrl($this->route),
						'method'=>'get',
						'id'=>'form-visible',
					));
					?>
					<div class="col-sm-2 pull-left page-sizes"  >

						<?php echo $form->dropDownListGroup(
							$model,
							'pagination_size',
							array(

								'widgetOptions'=>array(
									'data'=>$model->pages_size(),
									'htmlOptions'=>array(

									),
								),
								'hint'=>''
							)
						); ?>
					</div>
					<?php
					$this->endWidget();
					$this->widget('booster.widgets.TbGridView', array(
						'id' => 'user-grid',
						'type' => 'striped bordered condensed',
						'template' => '{pager}{items}{summary}',
						'enablePagination' => true,
						'pager' => array(
							'class' => 'booster.widgets.TbPager',
							'nextPageLabel' => 'Next',
							'prevPageLabel' => 'Previous',
							'htmlOptions' => array(
								'class' => 'pull-right'
							)
						),
						'htmlOptions' => array(
							'class' => 'table-responsive'
						),
						'dataProvider' => $model->search(),
						'filter' => $model,
						'columns' => array(

							array(
								'class' => 'booster.widgets.TbEditableColumn',
								'name' => 'name',
								'sortable' => true,
								'editable' => array(
									'url' => $this->createUrl('user/edit'),
									'placement' => 'right',
									'inputclass' => 'span1'
								),

								'visible'=>$model->visible_name?true:false,
							),
							array(
								'name'=>'username',
								'visible'=>$model->visible_username?true:false,
							),
							array(
								'class' => 'booster.widgets.TbEditableColumn',
								'name' => 'active',
								'type'=>'raw',
								'value'=>'CHtml::link($data->active?CHtml::tag("span",array("class"=>"label label-success"),"Active",true):CHtml::tag("span",array("class"=>"label label-danger"),"Disabled",true),"#",array("data-value"=>$data->active,"rel"=>"User_active","data-pk"=>$data->id,"class"=>"editable editable-click"))',
								'sortable' => true,
								'editable' => array(
									'type' => 'select2',
									'url' => $this->createUrl('user/edit'),
									'placement' => 'right',
									'inputclass' => 'span3',
									'source' => array('0'=>'Disabled','1'=>'Active'),
								),
								'visible'=>$model->visible_f_name?true:false,
							),
							array(
								'class' => 'booster.widgets.TbEditableColumn',
								'name' => 'gender',
								'type'=>'raw',
								'value'=>'CHtml::link($data->gender == "m" ?CHtml::tag("span",array("class"=>"label label-success"),"Male",true):CHtml::tag("span",array("class"=>"label label-warning"),"Female",true),"#",array("data-value"=>$data->active,"rel"=>"User_gender","data-pk"=>$data->id,"class"=>"editable editable-click"))',
								'sortable' => true,
								'editable' => array(
									'type' => 'select2',
									'url' => $this->createUrl('user/edit'),
									'placement' => 'right',
									'inputclass' => 'span3',
									'source' => array('f'=>'Female','m'=>'Male'),
								),
								'visible'=>$model->visible_f_name?true:false,
							),
							array(
							'name'=>'email',
								'type'=>'raw'
							),


							array(
								'name'=>'profile_pic',
								'type'=>'html',
 								'header'=>'Media',
								'htmlOptions'=>array('width'=>'150px'),
								'value'=>function($model){
									if(!empty($model->profile_pic)){
										echo CHtml::image($model->profile_pic,$model->profile_pic,array("class"=>"img-responsive img-tb-grid-view"));

									}else {
									if
										($model->gender == "m"){
											echo CHtml::image(Yii::app()->BaseUrl . '/temp/dist/img/avatar5.png', Yii::app()->BaseUrl . '/temp/dist/img/avatar5.png', array("class" => "img-responsive img-tb-grid-view"));
										}else{
											echo CHtml::image(Yii::app()->BaseUrl . '/temp/dist/img/avatar2.png', Yii::app()->BaseUrl . '/temp/dist/img/avatar2.png', array("class" => "img-responsive img-tb-grid-view"));
										}
									 }
								}
							),
						/*	array(
								'name'=>'created_at',
								'visible'=>$model->visible_created_at?true:false,
							),*/
 							array(
								'class' => 'booster.widgets.TbButtonColumn',
								'header' => 'Options',
								'template' => '{view}{update}{activate}{deactivate}{delete}',
								'buttons' => array(
									'view' => array(
										'label' => 'View',
										'icon' => 'fa fa-eye',
									),
									'update' => array(
										'label' => 'Update',
										'icon' => 'fa fa-pencil-square-o',
									),
									'delete' => array(
										'label' => 'Delete',
										'visible'=>'$data->id == Yii::app()->user->id ? false : true',
										'icon' => 'fa fa-times',
									),

								/*	'delete' => array(
										'label' => 'Delete',
										'icon' => 'fa fa-times',
									),*/

									'activate' => array(
										'label' => 'Deactivate',
										'url' => 'Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id."/status", array("id"=>$data->id))',
										'icon' => 'fa fa-toggle-off',
										'visible' => '($data->active == 1) and ($data->id == Yii::app()->user->id ? false : true)',
										'click' => "function(){

				$.fn.yiiGridView.update('user-grid', {
				type:'POST',
				url:$(this).attr('href'),
				success:function(data) {
				$.fn.yiiGridView.update('user-grid');}});return false;}",
									),
									'deactivate' => array(
										'label' => 'Activate',
										'url' => 'Yii::app()->createUrl(Yii::app()->controller->module->id."/".Yii::app()->controller->id."/status", array("id"=>$data->id))',
										'icon' => 'fa fa-toggle-on',
										'visible' => '($data->active == 0) and ($data->id == Yii::app()->user->id ? false : true)',
										'click' => "function(){
				$.fn.yiiGridView.update('user-grid', {
				type:'POST',
				url:$(this).attr('href'),
				success:function(data) {
				$.fn.yiiGridView.update('user-grid');}});return false;}"
									),

								)
							),						))); ?>
				</div>
			</div>
		</div>
	</div>




 </section>

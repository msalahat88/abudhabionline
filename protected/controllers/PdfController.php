<?php

class PdfController extends BaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */


	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','create','index','view'),
				'users'=>array(Yii::app()->user->getState('type')),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		$model->generated =2;
		$model->save();
		/*$file = Yii::app()->params['webroot'].$model->media_url;

		if(file_exists($file)){
			$html = explode('.',$file);
			if(file_exists($html[0].'.html')){
				unlink($file) ;
				unlink($html[0].'.html');
				$model->delete();
			}
		}*/
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Pdf;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);


		if(isset($_POST['Pdf']))
		{
			$valid = true;
			$model->attributes=$_POST['Pdf'];
			$model->created_at = date('Y-m-d H:i:s');
			if(strtotime($model->from_date) > strtotime($model->to_date)){
				$model->addError('from_date','Please check date ');
				$model->addError('to_date','Please check date ');
				$valid = false;
			}

			if($valid){
				$model->media_url = null;
				if($model->save(true))
				{
					Yii::app()->user->setFlash('success', "System will generate pdf file shortly .. ");
				}
			}
		}
		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function load_data($data){

		$items = array();
		foreach ($data as $item) {
			$items[] = $item->attributes;
		}
		return $items;
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Pdf('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Pdf'])) {
			$this->data_search = $_GET['Pdf'];
			$this->data_search($model);

			$model->attributes = $_GET['Pdf'];
		}
		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Pdf the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Pdf::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Pdf $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='pdf-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
